# PlantUML-Schema

This repository shows how to render an example schema using PlantUML. The related blog
post can be found [here](https://mcarl.in/blog/2019-09-22-customizing-plantuml-for-a-schema).

![Schema](schema_full.png)
